package com.javagda28.jsp.service;

import com.javagda28.jsp.database.EntityDao;
import com.javagda28.jsp.model.Grade;
import com.javagda28.jsp.model.GradeSubject;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

//public enum GradeService {
//    INSTANCE;
//
//    private List<Grade> grades = new ArrayList<>();
//
//    private static long NEXT_ID = 1000;
//
//    public void addGrade(Grade grade){
//        grade.setId(NEXT_ID++);
//        grade.setDateAdded(LocalDateTime.now());
//        grades.add(grade);
//    }
//
//    public void removeGrade(final Long id){
//        grades = grades.stream()
//                .filter(gr -> !gr.getId().equals(id))
//                .collect(Collectors.toList());
//    }
//
//    public List<Grade> getGrades() {
//        return grades;
//    }
//
//    public Optional<Grade> getGradeWithIndex(Long id) {
//        return grades.stream()
//                .filter(gr -> gr.getId().equals(id))
//                .findFirst();
//    }
//
//    public void update(Long identifier, HttpServletRequest req) {
//        for (Grade grade : grades) {
//            if(grade.getId().equals(identifier)){
//                grade.setSubject(GradeSubject.valueOf(req.getParameter("subject")));
//                grade.setValue(Double.parseDouble(req.getParameter("value")));
//                break;
//            }
//        }
//    }
//}
public class GradeService{
    public EntityDao entityDao = new EntityDao();

    public List<Grade> getGrades(){
        return entityDao.getAll(Grade.class);
    }

    public void saveGrade(Grade grade) {
        entityDao.saveOrUpdate(grade);
    }

    public void removeGrade(Long identifier) {
        entityDao.delete(Grade.class, identifier);
    }

    public Optional<Grade> findById(Long identifier) {
        return entityDao.getById(Grade.class, identifier);
    }

    public Grade update(Long identifier, HttpServletRequest req) {
        Optional<Grade> gradeOptional = findById(identifier);
        if(gradeOptional.isPresent()){
            Grade grade = gradeOptional.get();

            grade.setSubject(GradeSubject.valueOf(req.getParameter("subject")));
            grade.setValue(Double.parseDouble(req.getParameter("value")));

            saveGrade(grade);
            return grade;
        }
        throw new RuntimeException("Unexpected exception.");
    }
}