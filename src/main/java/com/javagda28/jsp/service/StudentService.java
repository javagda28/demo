package com.javagda28.jsp.service;

import com.javagda28.jsp.database.EntityDao;
import com.javagda28.jsp.model.Student;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

//public enum StudentService {
//    INSTANCE;
//
//    private List<Student> students = new ArrayList<>();
//
////    private static long NEXT_ID = 1000;
////    użycie: nowyRekord.setId(NEXT_ID++);
////
////    StudentSubject.valueOf(req.getParameter("subject"))
//
//
//    public void addStudent(Student student){
//        //
//        students.add(student);
//    }
//
//    public void removeStudent(final String indexNumber){
//        students = students.stream()
//                .filter(student -> !student.getIndexNumber().equals(indexNumber))
//                .collect(Collectors.toList());
//    }
//
//    public List<Student> getStudents() {
//        return students;
//    }
//
//    public Optional<Student> getStudentWithIndex(String indexNumber) {
//        return students.stream()
//                .filter(student -> student.getIndexNumber().equals(indexNumber))
//                .findFirst();
//    }
//
//    public void update(String editedIndex, HttpServletRequest req) {
//        for (Student student : students) {
//            if(student.getIndexNumber().equals(editedIndex)){
//                student.setAge(Integer.parseInt(req.getParameter("age")));
//                student.setFirstName(req.getParameter("name"));
//                student.setLastName(req.getParameter("surname"));
//                student.setSuspended(req.getParameter("suspended")!= null && req.getParameter("suspended").equals("on"));
//
//                break;
//            }
//        }
//    }
//}
public class StudentService{
    public EntityDao entityDao = new EntityDao();

    public List<Student> getStudents(){
        return entityDao.getAll(Student.class);
    }

    public void saveStudent(Student student) {
        entityDao.saveOrUpdate(student);
    }

    public void removeStudent(Long identifier) {
        entityDao.delete(Student.class, identifier);
    }

    public Optional<Student> findById(Long identifier) {
        return entityDao.getById(Student.class, identifier);
    }

    public void update(Long identifier, HttpServletRequest req) {
        Optional<Student> studentOptional = findById(identifier);
        if(studentOptional.isPresent()){
            Student student = studentOptional.get();

            student.setAge(Integer.parseInt(req.getParameter("age")));
            student.setFirstName(req.getParameter("name"));
            student.setLastName(req.getParameter("surname"));
            student.setSuspended(req.getParameter("suspended")!= null && req.getParameter("suspended").equals("on"));

            saveStudent(student);
        }
    }
}