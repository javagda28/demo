package com.javagda28.jsp.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data // getter setter, tostring hashcode equals
@AllArgsConstructor
@NoArgsConstructor
public class Student implements IBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // identyfikator jest generowany przez bazę
    private Long id;
    private String firstName;
    private String lastName;
    private String indexNumber;
    private int age;
    private boolean suspended;

    // metoda equals i hashcode jest nadpisana ponieważ używamy adnotacji @Data która zawiera @EqualsHashCode.
    // z tego powodu hashcode generowany jest na podstawie WSZYSTKICH pól, w tym kolekcji GRADE. Żeby obliczyć
    // hashcode obiektu student, należy obliczyć hashcode wszystkich ocen. Wewnątrz oceny również mamy taką samą adnotację
    // co sprawia że żeby obliczyć hashcode studenta musimy obliczyć hashcode oceny i metoda się zapętla
    // rekurencyjnie aż do wystąpienia "Stack overflow"

    // to samo dzieje się z metodą tostring
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    private Set<Grade> grades;

    // 1. tworzymy konstruktor
    // 2. tworzymy wewnętrzną klasę statyczną (Builder)
    // 3. klikamy prawym na konstruktor
    // 4. refactor
    // 5. replace constructor with builder
    // 6. zaznaczamy opcję use existing (na dole)
    // 7. klikamy po prawej wyszukiwanie klasy
    // 8. na liście (lub w projekcie) wyszukujemy wcześniej stworzoną klasę builder
    // 9. submit (jeśli pojawi się pytanie o refactor to zgadzamy się)

    public static class StudentBuilder{

        private String firstName;
        private String lastName;
        private String indexNumber;
        private int age;
        private boolean suspended;

        public StudentBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public StudentBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public StudentBuilder setIndexNumber(String indexNumber) {
            this.indexNumber = indexNumber;
            return this;
        }

        public StudentBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public StudentBuilder setSuspended(boolean suspended) {
            this.suspended = suspended;
            return this;
        }

        public Student createStudent() {
            return new Student(null, firstName, lastName, indexNumber, age, suspended, new HashSet<>());
        }
    }

}
