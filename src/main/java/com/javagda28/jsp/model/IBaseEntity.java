package com.javagda28.jsp.model;

public interface IBaseEntity {
    Long getId();
}
