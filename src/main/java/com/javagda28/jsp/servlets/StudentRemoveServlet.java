package com.javagda28.jsp.servlets;

import com.javagda28.jsp.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/student-remove")
public class StudentRemoveServlet extends HttpServlet {
    private final StudentService studentService = new StudentService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // pobieram od użytkownika parametr (indeks)
        Long id = Long.valueOf(req.getParameter("id"));

        // usuwam studenta z listy
        studentService.removeStudent(id);

        // przekierowuje użytkownika na stronę z listą studentów
        resp.sendRedirect("/students");
    }
}
