package com.javagda28.jsp.servlets;

import com.javagda28.jsp.model.Grade;
import com.javagda28.jsp.model.GradeSubject;
import com.javagda28.jsp.service.GradeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/grade-edit")
public class GradeEditServlet extends HttpServlet {
    private final GradeService gradeService = new GradeService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("gradeId"));

        // wyszukiwanie oceny (po LONG! identifier)
        Optional<Grade> gradeOptional = gradeService.findById(identifier);
        if(gradeOptional.isPresent()) {
//            // przekazuję ocene do edycji
            req.setAttribute("grade", gradeOptional.get());

            // przekazuję przedmioty (żeby dostępna była lista rozwijana przedmiotów)
            req.setAttribute("subjects", GradeSubject.values());

            // załadowanie widoku
            req.getRequestDispatcher("/grade-form.jsp").forward(req, resp);
        }else{
            // podobnie jak przy studentach, przekierowanie na listę ocen jeśli nie odnajdziemy oceny (może ktoś ją usunął?)
            resp.sendRedirect("/students");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("gradeId"));

        Grade grade = gradeService.update(identifier, req);

        resp.sendRedirect("/grades?studentId=" + grade.getStudent().getId());
    }
}
