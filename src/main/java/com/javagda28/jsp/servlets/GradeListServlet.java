package com.javagda28.jsp.servlets;

import com.javagda28.jsp.model.Student;
import com.javagda28.jsp.service.GradeService;
import com.javagda28.jsp.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/grades")
public class GradeListServlet extends HttpServlet {
    private final GradeService gradeService = new GradeService();
    private final StudentService studentService = new StudentService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long studentId = Long.parseLong(req.getParameter("studentId"));

        Optional<Student> studentOptional = studentService.findById(studentId);
        if(studentOptional.isPresent()) {
            Student owner = studentOptional.get();
            req.setAttribute("grades", owner.getGrades());
            req.setAttribute("owner", owner.getFirstName() + " " + owner.getLastName());
            req.getRequestDispatcher("/grade-list.jsp").forward(req, resp);
        }else{
            resp.sendRedirect("/students");
        }
    }
}
