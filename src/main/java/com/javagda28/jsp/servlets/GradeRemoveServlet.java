package com.javagda28.jsp.servlets;

import com.javagda28.jsp.service.GradeService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/grade-remove")
public class GradeRemoveServlet extends HttpServlet {
    private final GradeService gradeService = new GradeService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long identifier = Long.parseLong(req.getParameter("gradeId"));

        gradeService.removeGrade(identifier);

        resp.sendRedirect(req.getHeader("referer"));
    }
}
