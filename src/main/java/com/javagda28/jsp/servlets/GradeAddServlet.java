package com.javagda28.jsp.servlets;

import com.javagda28.jsp.model.Grade;
import com.javagda28.jsp.model.GradeSubject;
import com.javagda28.jsp.model.Student;
import com.javagda28.jsp.service.GradeService;
import com.javagda28.jsp.service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet("/grade-add")
public class GradeAddServlet extends HttpServlet {
    private final GradeService gradeService = new GradeService();
    private final StudentService studentService = new StudentService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("studentToWhomIAddGrade", req.getParameter("studentId"));
        req.setAttribute("subjects", GradeSubject.values());
        req.getRequestDispatcher("/grade-form.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Grade grade = extractGradeFromParameters(req);
        Long studentId = Long.parseLong(req.getParameter("studentIdentifier"));
        Optional<Student> studentOptional = studentService.findById(studentId);
        if(studentOptional.isPresent()) {
            grade.setStudent(studentOptional.get());
//            studentOptional.get().getGrades().add(grade);
            gradeService.saveGrade(grade);

            // wyświetl tylko oceny tego studentas
            resp.sendRedirect("/grades?studentId=" + studentId);
        }else{
            resp.sendRedirect("/students");
        }
    }

    private Grade extractGradeFromParameters(HttpServletRequest req) {
        // do tego stworzyłem dodatkowy konstruktor w klasie Grade
        return new Grade(
                Double.parseDouble(req.getParameter("value")),
                GradeSubject.valueOf(req.getParameter("subject")));
    }
}
