<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 12:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Calculate</title>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Calculator</h1>
<form action="/calculator" method="post">
    Liczba 1: <input type="number" step="0.01" name="pierwszaLiczba"> </br>
    Liczba 2: <input type="number" step="0.01" name="drugaLiczba"> </br>
    Dzialanie:<input type="text" name="dzialanie"> </br></br>
    <input type="submit">
</form>
<hr>

<%
    if(request.getAttribute("wynik") != null) {
        out.print("Twój wynik to: " + request.getAttribute("wynik"));
    }
%>

</body>
</html>
