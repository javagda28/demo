<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--Daje nam możliwość obsługi wpisów "${}" w kodzie. Bez tego nie możemy używać tej notacji i wszystkie użycia wypiszą pusty tekst--%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Student List</title>
    <style>
        td{
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Student List</h1>
<%--for(Student student : students) --%>

<table style="border: 1px solid #000; width: 100%">
    <tr>
        <th>Lp.</th>
        <th>First name</th>
        <th>Last name</th>
        <th>Index number</th>
        <th>Age</th>
        <th>Is suspended</th>
        <th></th>
        <th></th>
    </tr>
    <%--for(Student student : students) (gdzie students to atrybut przekazany w servlecie) --%>
<c:forEach var="student" items="${requestScope.students}" varStatus="loop">
    <tr>
        <td>${loop.index}</td>
        <td>${student.getFirstName()}</td>
        <td>${student.getLastName()}</td>
        <td>${student.getIndexNumber()}</td>
        <td>${student.getAge()}</td>
        <td>${student.isSuspended()}</td>
        <td>
            <a href="/student-remove?id=${student.getId()}">Remove</a>
        </td>
        <td>
            <a href="/student-edit?id=${student.getId()}">Edit</a>
        </td>
        <td>
            <a href="/grade-add?studentId=${student.getId()}">Add grade</a>
        </td>
        <td>
            <a href="/grades?studentId=${student.getId()}">List grades</a>
        </td>
    </tr>
</c:forEach>

</table>

<c:if test="">

</c:if>
</body>
</html>
