<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--Daje nam możliwość obsługi wpisów "${}" w kodzie. Bez tego nie możemy używać tej notacji i wszystkie użycia wypiszą pusty tekst--%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Grade List</title>
    <style>
        td{
            border: 1px solid #000;
        }
    </style>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Grade List of ${requestScope.owner}</h1>
<table style="border: 1px solid #000; width: 100%">
    <tr>
        <th>Lp.</th>
        <th>Id</th>
        <th>Value</th>
        <th>Subject</th>
        <th>Date added</th>
        <th></th>
        <th></th>
    </tr>
<c:forEach var="grade" items="${requestScope.grades}" varStatus="loop">
    <tr>
        <td>${loop.index}</td>
        <td>${grade.getId()}</td>
        <td>${grade.getValue()}</td>
        <td>${grade.getSubject()}</td>
        <td>${grade.getDateAdded()}</td>
        <td>
            <a href="/grade-remove?gradeId=${grade.getId()}">Usun</a>
        </td>
        <td>
            <a href="/grade-edit?gradeId=${grade.getId()}">Edit</a>
        </td>
    </tr>
</c:forEach>

</table>
</body>
</html>
