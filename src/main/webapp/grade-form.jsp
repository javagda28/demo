<%@ page import="com.javagda28.jsp.model.Student" %>
<%@ page import="com.javagda28.jsp.model.Grade" %><%--
  Created by IntelliJ IDEA.
  User: root
  Date: 11/23/19
  Time: 2:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Grade Form</title>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Grade Form</h1>
<%
    Grade grade = null;
    if(request.getAttribute("grade") != null){
        grade = (Grade) request.getAttribute("grade");
    }
%>
<c:if test="${requestScope.grade != null}">
    Editing grade that was added: ${requestScope.grade.getDateAdded()} with id: ${requestScope.grade.getId()}
</c:if>
<%--studentToWhomIAddGrade--%>
<form action="<%= grade != null ? "/grade-edit" : "/grade-add" %>" method="post">
    <input type="hidden" name="gradeId" value="${requestScope.grade.getId()}">
    <input type="hidden" name="studentIdentifier" value="${requestScope.studentToWhomIAddGrade}">
    Value: <input type="number" name="value" step="0.5" min="1" max="6" value="<%= grade != null ? grade.getValue() : "1" %>"/><br/>
    Subject: <select name="subject">
        <c:forEach var="sub" items="${requestScope.subjects}">
            <option value="${sub}" ${sub == requestScope.grade.getSubject() ? "selected" : ""} >${sub}</option>
        </c:forEach>
    </select><br/><br/>
    <input type="submit"/>
</form>
</body>
</html>
